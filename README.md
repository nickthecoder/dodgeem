# Dodge Em

Inspired by [Escapa!](http://members.iinet.net.au/~pontipak/redsquare.html)
Move your block with the mouse, and dodge out of the way of the nasties.
Games typically last less than 1 minute.
The graphics are dreadful, but the gameplay can be quite addictive!

![screenshot](screenshot.png)

To play the game, you must first install
[Tickle](https://nickthecoder.co.uk/software/tickle).

Start Tickle, then click "Play", and choose the file : `dodgeEm.tickle`.
(If you've set up the proper file associations for ".tickle" files, you can double click it instead).

Powered by [Tickle](https://nickthecoder.co.uk/software/tickle) and [LWJGL](https://www.lwjgl.org/).
