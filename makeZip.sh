#!/bin/sh

cd ..
rm dodgeem/out/dodgeem.zip

zip dodgeem/out/dodgeem.zip dodgeem -r -x  'dodgeem/.git/*' 'dodgeem/.gitignore' 'dodgeem/out/*' 'dodgeem/.gradle*' 'dodgeem/.*' 'dodgeem/src/*' 'dodgeem/makeZip.sh'
